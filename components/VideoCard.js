import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator
  } from 'react-native';
import VideoMetaRow from './VideoMetadataRow';

export default class VideoCard extends React.Component {
  constructor(props){
    super(props);
      this.state = {
        loading: true
      }
  }

  handleLoad = () => {
   this.setState({loading: false});
  };
  render(){
    const { loading } = this.state;
    const { image, title, videoId } = this.props;

  return (
     <View style={styles.container}>
     <Text style={styles.text} >{title}</Text>
        <View style={styles.image}>
          { loading && (
          <ActivityIndicator style={StyleSheet.absoluteFill} size={'large'} />
          )}
          <Image
          style={StyleSheet.absoluteFill}
          source={image}
          onLoad={this.handleLoad}
          />
          </View>
          <VideoMetaRow videoId={videoId}/>
      </View>
  );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
         backgroundColor: '#fff',
         padding: 5,
     },
 image: {
    aspectRatio: 2,
    backgroundColor: '#fff',
  },
   text: {
     fontWeight: 'bold',
     paddingVertical: 10,
     paddingHorizontal: 10
   }
});