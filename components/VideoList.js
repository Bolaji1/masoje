import React, { Component } from 'react';
import { FlatList,  StyleSheet,  View, Platform,   TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import VideoCard from './VideoCard';

const keyExtractor = ({ id }) => id.videoId.toString();

export default class VideoList extends Component {
   static propTypes = {
      items: PropTypes.arrayOf(
      PropTypes.shape({
      id: PropTypes.object.isRequired
      }),
      ).isRequired,
   };
   renderItem = ({ item: {snippet: { title, thumbnails:{medium: {url}}}, id}, separators}) => {
    return (
        <TouchableHighlight
        onPress={() => this._onPress(id)}
        onShowUnderlay={separators.highlight}
        onHideUnderlay={separators.unhighlight}
        containerStyle={styles.container}
        > 
            <VideoCard
            title={title}
            image={{
            uri: url,
            }}
            videoId={id.videoId}
            />
      </TouchableHighlight>
    );
   }

   renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "green",
          marginBottom: 20
        }}
      />
    );
  };

   _onPress({ videoId }){
    const { navigate } = this.props.navigation;
    navigate('YouTubeVideo', {youtubeId: videoId})
   }
  render(){
      const { items } = this.props;

    return(
        <FlatList
                ItemSeparatorComponent={this.renderSeparator}
                data={items}
                renderItem={this.renderItem}
            />
    )
    }
}
const styles = StyleSheet.create({
    container: {
       flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        padding: 30
    }
  });