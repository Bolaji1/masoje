import React from 'react';
import {
  StyleSheet,
  Text,
  View
  } from 'react-native';
  import Popularity from './Popularity'; 
  import { fetchVideoMetadata } from '../utils/YoutubeApi';

  export default class VideoMetaRow extends React.Component{
    constructor(props){
      super(props)
      this.state = {
        likeCount: null,
        viewCount: null
      }
    }
   async componentDidMount(){
     const { items } = await fetchVideoMetadata(this.props.videoId);
     const [ videoMetadata ] = items;
     const { likeCount, viewCount } = videoMetadata.statistics;
    
      this.setState({ viewCount, likeCount });
    }

    render(){
      return (
        <Popularity
        likes={this.state.likeCount}
        views={this.state.viewCount}
         />
   );
    }
}