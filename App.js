import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import VideoList from './components/VideoList';
import { createStackNavigator } from 'react-navigation';
import YoutubeVideoPlayer from './screens/YoutubePlayer';
import { fetchMovies } from './utils/YoutubeApi';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logo: {
    fontStyle: 'normal',
    paddingHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 35,
  }
});

 class App extends React.Component {
  static navigationOptions = {
    headerStyle : {
        backgroundColor: '#fff'
    },
    headerLeft:(
        <TouchableOpacity>
             <Text style={styles.logo}>Tiwa</Text>
        </TouchableOpacity>
  )
}
  constructor(props){
    super(props);
  
    this.state = {
       data: []
    }
  }

  async componentDidMount() {
    const movieArray = [];
    const movies = await fetchMovies();
      console.log(movies);
    movies.forEach((response) => {
      response.items.forEach((item) => {
        movieArray.push(item);
      }); 
    });
  
           this.setState({
                data: movieArray
             });
   }

  render() {
    return (
      <View style={styles.container}>
      <VideoList {...this.props} items={this.state.data}/>
      </View>
    );
  }
}

const screens = createStackNavigator({
  Home: {screen: App },
  YouTubeVideo: { screen: YoutubeVideoPlayer }
});

export default screens;