
import getChannelIds from './getChannellds';

const apiKey ='AIzaSyAw5SyOK3rODFQHimTHLTrQlb3-SS9sB_4';
const results = 30;


export const fetchMovies = async () => {
  
    try{
    const fetchPromises = getChannelIds().map((channelId) => {
            return fetch(`https://www.googleapis.com/youtube/v3/search/?key=${apiKey}&channelId=${channelId}&part=snippet,id&order=date&maxResults=${results}`)
            .then((response) => response.json())
       });

   return await Promise.all(fetchPromises);

    }catch(e){
      console.log(e);
    }

}

export const fetchVideoMetadata = async (videoId) => {
    try{
     const response = await fetch(`https://www.googleapis.com/youtube/v3/videos?key=${apiKey}&id=${videoId}&part=snippet,statistics,contentDetails`)

     const videoMetadata = await response.json();
     return videoMetadata;
    }catch(e){
        console.log(e);
    }
}

export async function fetchPlayListId(){
    try{
       const playlistIdsPromises = getChannelIds().map((channelId) => {
          return fetch(`https://www.googleapis.com/youtube/v3/channels/?key=${apiKey}&id=${channelId}&part=contentDetails`)
          .then((response) => response.json())
       });
      const response = await Promise.all(playlistIdsPromises);

      return response.map(({ items }) => items[0].contentDetails.relatedPlaylists.uploads);
    }catch(e){

    }
}