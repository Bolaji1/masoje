
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Youtube from 'react-native-youtube';

export default class YoutubeVideo extends Component {
    static navigationOptions = {
        headerTitle: 'Youtube',
        headerStyle: {
            backgroundColor: '#000'
        },
        headerTitleStyle: {
            color:'#fff'
        },
    }
    render(){
        return(
            <View style={styles.container}>
                <Youtube apiKey={'AIzaSyAw5SyOK3rODFQHimTHLTrQlb3-SS9sB_4'}
                         play={true}
                         fullscreen={true}
                         loop={true}
                         videoId={this.props.navigation.state.params.youtubeId}
                         onReady={e => this.setState({ status: e.state})}
                         onChangeState={e => this.setState({ status: e.state})}
                         onChangeQuality={e => this.setState({ quality: e.quality})}
                         onError={e => this.setState({ error: e.error})}
                         style={{alignSelf: 'stretch', height: 300 }}
                />
            </View>
                )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    }
})